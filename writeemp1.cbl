       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITEEMP1.
       AUTHOR. AREEYEO.
       
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.
       FD  EMP-FILE.
       01  EMP-DETAILS.
           88 END-OF-END-FILE   VALUE HIGH-VALUE.
           05 EMP-SSN           PIC 9(9).
           05 EMP-NAME.
              10 EMP-SURNAME       PIC X(15).
              10 EMP-FORNAME       PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              10 EMP-YOB           PIC 9(4).
              10 EMP-MOB           PIC 9(2).
              10 EMP-DOB           PIC 9(2).
           05 EMP-GENDER        PIC X.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT EMP-FILE
           MOVE "123456789" TO EMP-SSN
           MOVE "DENGJAROEN" TO EMP-SURNAME 
           MOVE "AREEYA" TO EMP-FORNAME
           MOVE "20001011" TO EMP-DATE-OF-BIRTH
           MOVE "W" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "987654321" TO EMP-SSN
           MOVE "DENGJAROEN" TO EMP-SURNAME 
           MOVE "AREEYA" TO EMP-FORNAME
           MOVE "19970327" TO EMP-DATE-OF-BIRTH
           MOVE "W" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "123456789" TO EMP-SSN
           MOVE "KIM" TO EMP-SURNAME 
           MOVE "TAEHYUNG" TO EMP-FORNAME
           MOVE "19951230" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           MOVE "987654321" TO EMP-SSN
           MOVE "MIN" TO EMP-SURNAME 
           MOVE "YOONGI" TO EMP-FORNAME
           MOVE "19930327" TO EMP-DATE-OF-BIRTH
           MOVE "M" TO EMP-GENDER
           WRITE EMP-DETAILS

           CLOSE EMP-FILE
           GOBACK
           .
